package ru.template.handler

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import org.springframework.web.socket.BinaryMessage
import org.springframework.web.socket.CloseStatus
import org.springframework.web.socket.TextMessage
import org.springframework.web.socket.WebSocketSession
import org.springframework.web.socket.handler.BinaryWebSocketHandler
import org.springframework.web.socket.handler.TextWebSocketHandler
import java.nio.ByteBuffer
import java.util.*


/**
 * Created by Andrey on 16.05.2016.
 */
@Component
class SocketHandler: TextWebSocketHandler() {

    var logger: Logger = LoggerFactory.getLogger(this.javaClass);

    lateinit var session :WebSocketSession;
    var sessions: MutableList<WebSocketSession> = arrayListOf()

    override fun handleTransportError(session: WebSocketSession?, exception: Throwable?) {
        System.out.print(session);
    }

    override fun afterConnectionClosed(session: WebSocketSession?, status: CloseStatus?) {
        logger.info("closed "+status);
        sessions.remove(session);
    }

    override fun afterConnectionEstablished(session: WebSocketSession?) {
        this.session = session!!;
        sessions.add(session);
        super.afterConnectionEstablished(session);
    }

    override fun handleTextMessage(session: WebSocketSession, message: TextMessage) {
        var time = Date().time;
        var jsonObject = ObjectMapper().readTree(message.payload);
        var msg = BinaryMessage(Base64.getDecoder().decode(jsonObject.findValue("data").textValue()));
        sessions.forEach { s ->
            if(session != s){
                s.sendMessage(msg)
                logger.info((Date().time - time).toString() + " ");
            }
        }
    }

    override fun handleBinaryMessage(session: WebSocketSession, message: BinaryMessage) {
        var time = Date().time;
        var msg = BinaryMessage(message.payload.array());
        sessions.forEach { s ->
            if(session != s){
                s.sendMessage(msg)
                logger.info((Date().time - time).toString() + " ");
            }
        }
    }

}