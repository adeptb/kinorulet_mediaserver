package ru.template.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.messaging.simp.config.MessageBrokerRegistry
import org.springframework.web.socket.config.annotation.*
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean
import ru.template.handler.SocketHandler


/**
 * Created by Andrey on 15.05.2016.
 */
@Configuration
@EnableWebSocket
open class WebSocketConfig : WebSocketConfigurer {

    @Autowired
    lateinit var socketHandler: SocketHandler;

    override fun registerWebSocketHandlers(registry : WebSocketHandlerRegistry?) {
        registry!!.addHandler(socketHandler, "/greeting").setAllowedOrigins("*");
    }

    @Bean
    open fun createWebSocketContainer(): ServletServerContainerFactoryBean {
        var container = ServletServerContainerFactoryBean();
        container.maxTextMessageBufferSize = 1048576*8;
        container.maxBinaryMessageBufferSize = 1048576*8;
        return container;
    }

}